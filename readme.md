# Eurovision Song Contest 1975-2019
This repository contains a shiny application to visualise which countries 
gives points to each other in the Eurovision Song Contest since 1975. 

## Data Preparation
To generate all data for further analysis or for running the shiny 
application it should be sufficient to run the `R/runAll.R` script which
should then generate all data sets that could be of interest for you. 
For some of the datasets the main idea behind saving them is so you
don't have to re-run the entire processing when changing something.

These data sets as follows:

* `esc_raw.Rds` - The raw data as `tbl`.
* `esc.Rds` -  The data with cleaning applied, but nothing regarding scores.
* `esc_average.Rds` - Data redcued to the required minimum and with scores
standardised.
* `esc_n` - Data aggregated across years including scaled scores and
information about common participations. This is probably the main data
of interest. 

Additionally there are to data sets containing maps, one higher-resolution
one showing all the participating countries and a lower-resolution one 
for the background. 

Some of these datasets are copied to the shiny directory to make the 
app standalone.

## Dependencies
The following packages are required to run this analysis:

* `here`
* `readxl`
* `rnaturalearth`
* `sf`
* `tidyverse` (if you want to keep things minimal, `dplyr`, `purrr`, `tibble`
    and `tidyr`)

To run the shiny app you obviously need `shiny`, also `shinythemes` and 
`ggiraph` and `ggplot2` from the `tidyverse`.

The Rmarkdown dcument requires `ggplot2` `tidygraph`, `ggraph` and
`ISOcodes` in addition to the main processing.

## Results
I have put an exploration of the data [on my website](https://hohenfeld.is/posts/analysing-points-in-the-eurovision-song-contest/), a slightly modified version of that post is also available in the `Rmd`
directory of this repository.

## Licence
All contents of this repository are licensed under CC BY-NC-SA 4.0. See file 
LICENCE for details.

## Data Source
The raw data for this repo was taken from Kaggle and is available in the 
`data-raw` directory.
https://www.kaggle.com/datagraver/eurovision-song-contest-scores-19752019

The raw data is licenced under CC BY-NC-SA 4.0.
